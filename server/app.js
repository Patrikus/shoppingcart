const express = require("express");
const path = require("path");
const fs = require("fs");
const cors = require("cors");

const PORT = 3030;

const app = express();

app.get("/api", cors(), (req, res, next) => {
  req.header("User-Agent");
  res.set("Content-Type", "text/json");
  const data = fs.readFileSync(path.join(__dirname, "products.json"));
  const stats = JSON.parse(data);
  res.json(stats);
});

app.get("/checkout", cors(), (req, res, next) => {
  res.send("proceeding to checkout");
});

app.listen(PORT, () => {
  console.log(`Server is listening on port: ${PORT}`);
});
