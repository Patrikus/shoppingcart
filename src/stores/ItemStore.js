import Vuex from "vuex";
import Vue from "vue";
import axios from "axios";
import { notification } from "ant-design-vue";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    total: 0,
    coupon: "",
    products: []
  },

  getters: {
    getTotal: state => {
      return state.total;
    },
    getCartProductsLength: state => {
      return state.products.reduce((a, b) => {
        return a + b["quantity"];
      }, 0);
    }
  },

  mutations: {
    changeProductQuantity(state, data) {
      state.products
        .filter(e => e.id == data.id)
        .map(el => (el.quantity = data.value));
    },
    removeProduct(state, id) {
      state.products.filter(e => e.id == id).map(el => (el.quantity = 0));
    },
    initialiseStore(state, data) {
      state.products.push(...data);
    },
    getSavedCart(state) {
      if (localStorage.getItem("cartStore")) {
        this.replaceState(
          Object.assign(state, JSON.parse(localStorage.getItem("cartStore")))
        );
      }
    },
    applyPromoCode(state, code) {
      state.coupon = code;
    },
    recalculateTotal(state) {
      let tempTotal = state.products.reduce((a, b) => {
        return a + b["price"] * b["quantity"];
      }, 0);
      state.total = state.coupon ? tempTotal - 10 : tempTotal;
      if (state.total < 0) {
        state.total = 0;
      }
    }
  },

  actions: {
    changeQuantity(context, data) {
      context.commit("changeProductQuantity", data);
      context.commit("recalculateTotal");
    },
    removeProduct(context, id) {
      context.commit("removeProduct", id);
    },
    saveStore(context) {
      context.commit("recalculateTotal");
      localStorage.setItem("cartStore", JSON.stringify(context.state));
    },
    applyPromoCode(context, code) {
      context.commit("applyPromoCode", code);
      context.commit("recalculateTotal");
    },

    async initialiseStore(context) {
      const productsTemp = await axios.get("//localhost:3030/api");
      context.commit("initialiseStore", productsTemp.data);
      context.commit("getSavedCart");
    },

    async proceedToCheckout(context) {
      if (context.state.total > 0) {
        const checkout = await axios.get("//localhost:3030/checkout");
        notification.open({
          message: "Checkout",
          description: `Respond from the server - ${checkout.data}`
        });
      }
    }
  }
});
